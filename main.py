from typing import Union


def get_user_age():
    user_input = input('Enter your age (int numbers only) >>> ')
    user_age = int(user_input)
    return user_age


def write_data_to_file(some_data: str, name: str = 'Gosha'):
    with open(f'name {name}.txt', mode='w', encoding='utf-8') as file:
        file.write(some_data)


def is_number_even(number: Union[int, float]) -> bool:
    n = number % 2
    # print(bool(n))  # Fasle - '', 0, 0.0, [], False, None
    if n:
        return False
    return True


def multiply_to_two(value: Union[int, float, str, list]) -> Union[int, float, str, list]:
    """
    provide multiplying to two
    Args:
        value (int|float|str|list): given value

    Returns:
        (int|float|str|list): result of processing value
    """
    result = value * 2
    return result


print(multiply_to_two(2))
