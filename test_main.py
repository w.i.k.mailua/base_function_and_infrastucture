import main


def test_multiply_to_two_int():
    result = main.multiply_to_two(2)
    assert result == 4, 'Wrong result'
    assert result is 4, 'Wrong result'


def test_multiply_to_two_float():
    result = main.multiply_to_two(2.2)
    assert result == 4.4, 'Wrong result'
    assert type(result) == float, 'Wrong result'


def test_is_even_true():
    result = main.is_number_even(2)
    assert result is True


def test_is_even_false():
    result = main.is_number_even(3)
    assert result is False
